*** Settings ***
Library  Selenium2Library


*** Variables ***
${email_id}    email
${password_id}     pass
${loginBtn_id}     loginbutton
${profile_id}      .fbxWelcomeBoxName
${postTimeline}     //*[@data-testid="react-composer-root"]
${optionView}   ._55pe
${optionPublic}     ._48u1
${addPost}  ._1mf7
${userNavigation}  pageLoginAnchor
${logout}   div.uiScrollableAreaWrap > div > div > ul > li:nth-child(12)
${postContent}  .userContent
${postMenu}     ._4r_y .uiPopover
${deletePost}   .uiContextualLayerBelowRight > div > div > ul > li:nth-child(1)
${confirmDelete}    .layerConfirm
${postText}     Just In school.
${facebook}     https://facebook.com
${changeProfile}    .fbxWelcomeBoxSmallLeft
${choosePicture}    .fbTimelineProfilePicSelector > div > a
${inputPicture}     //*[@data-testid="upload_photo_button"]/div/input
${savePicture}  //*[@data-testid="profilePicSaveButton"]
${timelineTextArea}     //*[@data-testid="status-attachment-mentions-input"]
${pictureSelector}      .fbTimelineProfilePicSelector > div > a



*** Keywords ***
Login To Facebook
    wait until element is visible  ${email_id}
    input text  id=${email_id}     meqqiuj_putnamwitz_1476164268@tfbnw.net
    wait until element is visible  ${password_id}
    input password  id=${password_id}  5lbvlluisvs
    wait until element is visible  ${loginBtn_id}
    click element  id=${loginBtn_id}
    wait until element is visible  css=${profile_id}
    element text should be  css=${profileid}  Mike Alacgghaiifie Putnamwitz

Add post to timeline
    #wait until element is visible  xpath=//*[@data-testid="react-composer-root"]
    #click element  xpath=${timelineTextArea}
    wait until element is visible  id=feedx_container
    click element  id=feedx_container
    wait until element is visible  css=._4c_p
    click element  css=._4c_p
    wait until element is visible  xpath=${timelineTextArea}
    input text  xpath=${timelineTextArea}    ${postText}
    click element  css=${optionView}
    wait until element is visible  css=${optionPublic}
    click element  css=${optionPublic}
    wait until element is visible  css=${addPost}
    click button  css=${addPost}

Delete post from wall
    #sleep  5s
    wait until element is visible  css=${postMenu}
    #sleep  2s
    click element  css=${postMenu}
    wait until element is visible  css=${deletePost}
    click element  css=${deletePost}
    wait until element is visible  css=${confirmDelete}
    click element  css=${confirmDelete}
    wait until page does not contain  ${postText}  10s

Logout From Facebook
    wait until element is visible  id=${userNavigation}
    #sleep  5s
    click element  id=${userNavigation}
    wait until element is visible  css=${logout}    10s
    click element  css=${logout}
    wait until element is visible  id=${email_id}
    wait until element is visible  id=${password_id}

Open and maximize browser
    [Arguments]     ${url}  ${browser}
    open browser  ${url}    ${browser}
    maximize browser window

I am logged in to facebook
    Open and maximize browser  ${facebook}  ff
    login to facebook

I add post to timeline
    Add post to timeline

post on timeline is correctly added
    wait until page contains    ${postText}     10s
    #Delete post from wall

post is safely deleted
    Delete post from wall

I changed my profile picture
    click element  css=${changeProfile}
    wait until element is visible  css=${pictureSelector}
    click element  css=${pictureSelector}
    wait until element is visible  xpath=${inputPicture}
    choose file  xpath=${inputPicture}    ${CURDIR}/profile.png
    wait until element is visible  xpath=${savePicture}  10s
    click element  xpath=${savePicture}

Profile picture is correctly added
    sleep  1s
    wait until page contains  Mike Alacgghaiifie Putnamwitz si aktualizoval profilový obrázek.

I have login facebook page
    Open and maximize browser  ${facebook}  ff