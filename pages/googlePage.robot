*** Settings ***
Documentation    googlePage object
Library  Selenium2Library
Library  BuiltIn


*** Variables ***
${seznamUrl}    http://www.seznam.cz
${vseUrl}    https://www.vse.cz/
${googleUrl}    http://google.cz/
${searchField}  lst-ib
${searchBtn}    btnK
${searchBtnGoogle}  btnG
${results}   rso
${firstMatch}   div.g > div > div > h3
${logoVse}  logo
${logoGoogle}   hplogo

*** Keywords ***
goto google page and check google logo
    Open Browser    ${googleUrl}    ff
    Maximize Browser Window
    wait until element is visible   id=${logoGoogle}
    wait until element is visible  id=${searchField}

Search For
    [Arguments]  ${text}
    input text  ${searchField}  ${text}
    click element   name=${searchBtnGoogle}
    wait until element is visible  id=${results}
    wait until element is visible  css=${firstMatch}

put VSE to google search engine
    search for  VSE

search results should contains VSE page
    element should contain  css=${firstMatch}   VŠE: Vysoká škola ekonomická v Praze
    click element  css=${firstMatch}
    wait until element is visible  id=logo
    Sleep  2s
    location should be  ${vseUrl}

Browser with google engine
    goto google page and check google logo


