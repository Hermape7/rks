*** Settings ***
Documentation    Search VSE on google search engine and check if results of searching contents VSE.
Library  Selenium2Library
Resource  ../pages/googlePage.robot
Resource  ../lib/firefoxDriver.robot
Test Setup  setup driver
Test Teardown  Close Browser

*** Test Cases ***
Search VSE on google
    Given Browser with google engine
    When put VSE to google search engine
    Then search results should contains VSE page

