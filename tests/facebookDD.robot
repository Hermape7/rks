*** Settings ***
Documentation    Suite description
Library  Selenium2Library
Resource  ../lib/firefoxDriver.robot
Test Setup  setup driver
Test Teardown  Close browser
Test Template  login template
Force Tags  DataDriven  ValidTest

*** Variables ***
${facebook}     https://facebook.com
${email_id}    email
${password_id}     pass
${loginBtn_id}     loginbutton
${profile_id}      .fbxWelcomeBoxName

*** Testcases ***   Username            Password        CheckName
Login elizabeth     100014019550848     8175jipw6ng     Elizabeth Aladjaieejhdh Warmanwitz
login mike          100013778199695     5lbvlluisvs     Mike Alacgghaiifie Putnamwitz
login charlie       100014123859024     l439eri3t9s     Charlie Aladabcheijbd Sharpesky


*** Keywords ***
login template  [Arguments]  ${username}  ${password}  ${name}
    open browser  ${facebook}  firefox
    wait until element is visible  ${email_id}
    input text  id=${email_id}     ${username}
    wait until element is visible  ${password_id}
    input password  id=${password_id}  ${password}
    wait until element is visible  ${loginBtn_id}
    click element  id=${loginBtn_id}
    wait until element is visible  css=${profile_id}
    element text should be  css=${profileid}  ${name}

