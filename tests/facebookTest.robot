*** Settings ***
Documentation    Suite description
Library  Selenium2Library
Resource  ../pages/facebookPage.robot
Resource  ../lib/firefoxDriver.robot
Test Setup  setup driver
Test Teardown  Close browser

*** Test Cases ***
Scenario: Login to facebook and add timeline post
    [Tags]  timeline
    Given I am logged in to facebook
    When I add post to timeline
    And post on timeline is correctly added
    Then post is safely deleted

Scenario: Change profile picture on facebook
    [Tags]  profile
    Given I am logged in to facebook
    When I changed my profile picture
    Then Profile picture is correctly added

